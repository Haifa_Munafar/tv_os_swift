//
//  Data.swift
//  PhotoGellery_TV
//
//  Created by Fathima Haifa on 2/19/22.
//

import Foundation

class LocalData {
   public static let shared = LocalData()
    
    var imageArray: [ImageDetails] = []
    
    func getImageArray() -> [ImageDetails]  {
        let images = [ImageDetails(name: "Sunflower", source: "Sunflower"),
                      ImageDetails(name: "Lotus", source: "Lotus"),
                      ImageDetails(name: "Jesmin", source: "Jesmin"),
                      ImageDetails(name: "Tulips", source: "Tulip"),
                      ImageDetails(name: "Rose", source: "Rose"),
                      ImageDetails(name: "Shoe flower", source: "ShoeFlower")]
        return images
    }
}
