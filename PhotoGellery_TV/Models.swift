//
//  Models.swift
//  PhotoGellery_TV
//
//  Created by Fathima Haifa on 2/19/22.
//

import Foundation

struct ImageDetails {
    var name: String?
    var source: String?
}
