//
//  ViewController.swift
//  PhotoGellery_TV
//
//  Created by Fathima Haifa on 2/19/22.
//

import UIKit

class ImageViewController: UIViewController {
    
    
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var gelleryIMG: UIImageView!
    
    var imagesArray: [ImageDetails] = []
    var selectedIMG = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        imageCollection.delegate = self
        imageCollection.dataSource = self
        
        getImages()
    }

    func getImages() {
        imagesArray = LocalData.shared.getImageArray()
        selectedIMG = imagesArray.first?.source ?? ""
        gelleryIMG.image = UIImage(named: selectedIMG)
        imageCollection.reloadData()
    }

}

extension ImageViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imageCollection.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.ImageName.text = imagesArray[indexPath.row].name
        cell.image.image = UIImage(named: imagesArray[indexPath.row].source ?? "")
        cell.image.layer.cornerRadius = 16
        cell.image.layer.borderColor = UIColor.white.cgColor
        cell.image.layer.borderWidth = 1
        if imagesArray[indexPath.row].source == selectedIMG {
            cell.ImageName.textColor = UIColor.yellow
        } else {
            cell.ImageName.textColor = UIColor.white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIMG = imagesArray[indexPath.row].source ?? ""
        gelleryIMG.image = UIImage(named: selectedIMG)
        imageCollection.reloadData()
    }
}

extension ImageViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
   
            let size = imageCollection.frame.size
            return CGSize(width: ((size.width ) / 2 - 50), height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      
            return 8
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       
            return 2
        
    }
}

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var ImageName: UILabel!
}
